package facci.pm.realmdb;

public enum AccessorieStatus {

    Open("Open"),
    InProgress("In Progress"),
    Complete("Complete");
    String displayName;
    AccessorieStatus(String displayName) {
        this.displayName = displayName;
    }

}
