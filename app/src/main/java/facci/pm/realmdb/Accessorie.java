package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Accessorie extends RealmObject {

    @Required
    private String name;

    @Required
    private String status = AccessorieStatus.Open.name();

    @Required
    private String description;
    @Required
    private String quantity;
    @Required
    private String price;


    public Accessorie(String _name, String _description, String _quantity , String _price) {
        this.name = _name;
        this.description = _description;
        this.quantity = _quantity;
        this.price = _price;
    }
    public Accessorie() {}

    public void setStatus(AccessorieStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    public String getName() { return name; }

    public String getDescription() { return description; }
    public String getQuantity() { return quantity; }
    public String getPrice() { return price; }


    public void setName(String name) { this.name = name; }

    public void setDescription(String description) { this.description = description; }
    public void setQuantity(String quantity) { this.quantity = quantity; }
    public void setPrice(String price) { this.price = price; }

}
