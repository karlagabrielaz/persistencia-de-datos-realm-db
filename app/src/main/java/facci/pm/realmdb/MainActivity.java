package facci.pm.realmdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm uiThreadRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this); // context, usually an Activity or Application

        //Open a Realm
        String realmName = "PROYECTO REALM CON TABLAS";
        RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
        uiThreadRealm = Realm.getInstance(config);

        addChangeListenerToRealm(uiThreadRealm);

        FutureTask<String> Accessorie = new FutureTask(new BackgroundQuickStart(), "test");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(Accessorie);
    }

    //adjuntamos un elemento personalizado OrderedRealmCollectionChangeListener con el addChangeListener()
    private void addChangeListenerToRealm(Realm realm) {
        // all Tasks in the realm
        RealmResults<Accessorie> Accessories = uiThreadRealm.where(Accessorie.class).findAllAsync();
        Accessories.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Accessorie>>() {
            @Override
            public void onChange(RealmResults<Accessorie> collection, OrderedCollectionChangeSet changeSet) {
                // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate
                OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
                for (OrderedCollectionChangeSet.Range range : deletions) {
                    Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                for (OrderedCollectionChangeSet.Range range : insertions) {
                    Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                for (OrderedCollectionChangeSet.Range range : modifications) {
                    Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // the ui thread realm uses asynchronous transactions, so we can only safely close the realm
        // when the activity ends and we can safely assume that those transactions have completed
        uiThreadRealm.close();
    }

    public class BackgroundQuickStart implements Runnable {
        @Override
        public void run() {
            String realmName = "PROYECTO REALM CON TABLAS";
            RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
            Realm backgroundThreadRealm = Realm.getInstance(config);

            //To create a new Task, instantiate an instance of the Task class and add it to the realm in a write block
            //creamos objetos de la clase que trabajan como modelo
            Accessorie accessorie1 = new Accessorie("Reloj 1", "Reloj de acero", "5", "20");
            Accessorie accessorie2 = new Accessorie("Pulsera 2", "Pulsera de plata", "8", "12");

            //ejecutamos la transacción
            backgroundThreadRealm.executeTransaction (transactionRealm -> {
                transactionRealm.insert(accessorie1);
                transactionRealm.insert(accessorie2);
            });

            //You can retrieve a live collection of all items in the realm
            // all Tasks in the realm
            //buscamos los elementos que hay en la tabla, trae una lista de objetos de tareas
            RealmResults<Accessorie> Accessories = backgroundThreadRealm.where(Accessorie.class).findAll();

            //MODIFICAR
            //To MODIFY a task, update its properties in a write transaction block
            Accessorie otherAccessorie = Accessories.get(0);
            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                //se busca en específico el objeto que hace referencia
                Accessorie innerOtherAccessorie = transactionRealm.where(Accessorie.class)
                        .equalTo("name", otherAccessorie.getName())
                        .equalTo("description", otherAccessorie.getDescription())
                        .equalTo("quantity", otherAccessorie.getQuantity())
                        .equalTo("price", otherAccessorie.getPrice())
                        .findFirst();

                //para modificar se utiliza los metodos relacionados  a la encapsulacion de cada uno de sus campos
                innerOtherAccessorie.setStatus(AccessorieStatus.Open);
            });


            //ELIMINAR
            //you can DELETE a task by calling the deleteFromRealm() method in a write transaction block:
            Accessorie yetAnotherAccessorie = Accessories.get(0);
            String yetAnotherAccessorieName = yetAnotherAccessorie.getName();
            String yetAnotherAccessorieDescription = yetAnotherAccessorie.getDescription();
            String yetAnotherAccessorieQuantity = yetAnotherAccessorie.getQuantity();
            String yetAnotherAccessoriePrice = yetAnotherAccessorie.getPrice();
//            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Accessorie innerYetAnotherAccessorie = transactionRealm.where(Accessorie.class)
                        .equalTo("_id", yetAnotherAccessorieName)
                        .equalTo("_id", yetAnotherAccessorieDescription)
                        .equalTo("_id", yetAnotherAccessorieQuantity)
                        .equalTo("_id", yetAnotherAccessoriePrice)
                        .findFirst();
                innerYetAnotherAccessorie.deleteFromRealm(); //para eliminar usa el método deleteFromRealm
            });



            for (Accessorie accessorie : Accessories
            ) {
                Log.e("Accessorie", "Name: " + accessorie.getName()
                        + " Status: " + accessorie.getStatus()
                        + " Description: " + accessorie.getDescription()
                        + " Quantity: " + accessorie.getQuantity()
                        +  "Price: " + accessorie.getPrice());
            }


            // you can also filter a collection
            //otros ejemplos de búsqueda
            // el name empieze con la letra R
            RealmResults<Accessorie> AccessoriesThatBeginWithR = Accessories.where().beginsWith("name", "R").findAll();
            //con estado abierto
            RealmResults<Accessorie> openAccessories = Accessories.where().equalTo("status", AccessorieStatus.Open.name()).findAll();



            // because this background thread uses synchronous realm transactions, at this point all
            // transactions have completed and we can safely close the realm
            //cerramos la conexion con respecto a la bdd
            backgroundThreadRealm.close();
        }
    }
}